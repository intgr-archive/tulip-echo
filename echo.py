#!/usr/bin/env python

import asyncio

# Erm, recved == sent currently
recved = 0
sent = 0
conns = 0
paused = 0


class EchoProtocol:
    def connection_made(self, trans):
        #print("Connected", trans)
        self.trans = trans
        self.is_paused = False

        global conns
        conns += 1

    def data_received(self, data):
        global sent, recved
        recved += len(data)
        self.trans.write(data)
        sent += len(data)

    def eof_received(self):
        pass
        #print("EOF")

    def connection_lost(self, err):
        print("Connection lost", err)

        global conns
        conns -= 1
        if self.is_paused:
            global paused
            paused -= 1

    def pause_writing(self):
        self.trans.pause_reading()
        assert not self.is_paused
        self.is_paused = True

        global paused
        paused += 1

    def resume_writing(self):
        self.trans.resume_reading()

        assert self.is_paused
        self.is_paused = False
        global paused
        paused -= 1


def pretty_size(size):
    # http://www.dzone.com/snippets/filesize-nice-units
    suffixes = [('',2**10), ('k',2**20), ('M',2**30), ('G',2**40), ('T',2**50)]
    for suf, lim in suffixes:
        if size > lim:
            continue
        else:
            return "%s %sB" % (round(size/float(lim/2**10),1), suf)


loop = asyncio.get_event_loop()

@asyncio.coroutine
def run():
    global sent, recved

    print("Running")
    #server = loop.create_server_serving(EchoProtocol, host='0.0.0.0', port=9119)
    server = yield from loop.create_server(EchoProtocol, host='0.0.0.0', port=9119)
    print("Serving", server)

    t0 = loop.time()
    while True:
        yield from asyncio.sleep(1)
        t1 = loop.time()
        diff = t1-t0
        print("recv %8s/s sent %8s/s over %.5f s, %4d conns %4d paused"
              % (pretty_size(recved/diff), pretty_size(sent/diff), diff, conns, paused))

        recved = 0
        sent = 0
        t0 = t1

loop.run_until_complete(run())
