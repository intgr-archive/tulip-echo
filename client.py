#!/usr/bin/env python

import asyncio
import sys

BUFSIZ = 32*1024
DEFAULT_CONNS = 10

BUF = b'\x00' * BUFSIZ

sent = 0
recved = 0
conns = 0
paused = 0


class FloodProtocol:
    def __init__(self):
        self.unpause = None
        # We're paused until we connect
        self.pause_writing()

    def connection_made(self, trans):
        self.trans = trans
        self.resume_writing()

        #print("Connected", trans)
        global conns
        conns += 1

    @asyncio.coroutine
    def flood(self):
        global sent
        while True:
            while not self.unpause:
                self.trans.write(BUF)
                sent += len(BUF)

            yield from self.unpause

    def data_received(self, data):
        global recved
        recved += len(data)

    def eof_received(self):
        print("EOF")

    def connection_lost(self, err):
        print("Connection lost", err)

        global conns
        conns -= 1
        if self.unpause:
            global paused
            paused -= 1

    def pause_writing(self):
        assert not self.unpause
        self.unpause = asyncio.Future()

        global paused
        paused += 1

    def resume_writing(self):
        assert self.unpause
        self.unpause.set_result(None)
        self.unpause = None

        global paused
        paused -= 1


def pretty_size(size):
    # http://www.dzone.com/snippets/filesize-nice-units
    suffixes = [('',2**10), ('k',2**20), ('M',2**30), ('G',2**40), ('T',2**50)]
    for suf, lim in suffixes:
        if size > lim:
            continue
        else:
            return "%s %sB" % (round(size/float(lim/2**10),1), suf)


loop = asyncio.get_event_loop()

@asyncio.coroutine
def status():
    global sent, recved

    t0 = loop.time()
    while True:
        yield from asyncio.sleep(1)
        t1 = loop.time()
        diff = t1-t0
        print("sent %8s/s recv %8s/s over %.5f s, %4d conns %4d paused"
              % (pretty_size(sent/diff), pretty_size(recved/diff), diff, conns, paused))

        sent = 0
        recved = 0
        t0 = t1

@asyncio.coroutine
def run(num):
    clients = []
    print("Spawning %d clients" % num)

    # Create all outbound connections in parallel \o/
    connects = (loop.create_connection(FloodProtocol, host='127.0.0.1', port=9119) for x in range(num))
    for fut in asyncio.as_completed(connects):
        trans, client = yield from fut
        #print("Client", client)
        clients.append(client)

    print("Done, flooding")

    # FIXME this is clearly broken, how do I launch .flood() from connection_made?
    cors = [c.flood() for c in clients]

    # yield from asyncio.wait(cors)
    for fut in asyncio.as_completed(cors):
        ret = yield from fut
        print("Returned", ret)

if __name__=='__main__':
    if len(sys.argv) > 1:
        num = int(sys.argv[1])
    else:
        num = DEFAULT_CONNS
    loop.run_until_complete(asyncio.gather(run(num), status()))
